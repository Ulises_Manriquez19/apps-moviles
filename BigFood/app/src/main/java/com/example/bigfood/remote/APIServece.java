package com.example.bigfood.remote;
import com.example.bigfood.Login;
import com.example.bigfood.Models.LoginModel;
import com.example.bigfood.Models.Post;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIServece {
    @POST("index.php?action=registrar&controller=Usuario")
    @FormUrlEncoded
    Call<Post> savePost(@Field("Correo") String Correo,
                        @Field("Nombre") String Nombre,
                        @Field("Contraseña") String Contraseña,
                        @Field("Numero") Integer Numero);
}