package com.example.bigfood;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Locales extends AppCompatActivity {

    private EditText telefono, telefono1, telefono2;
    private Button btntelefono, btntelefono1, btntelefono2, btnmapa, btnmapa1, btnmapa2;

    private final int PHONE_CALL_CODE = 100;
    private  final int PHONE_CALL_CODE1=110;
    private  final int PHONE_CALL_CODE2=120;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locales);

        telefono = (EditText)findViewById(R.id.telefono);
        telefono1 = (EditText)findViewById(R.id.telefono1);
        telefono2 = (EditText)findViewById(R.id.telefono2);

        btntelefono = (Button)findViewById(R.id.btntelefono);
        btntelefono1 = (Button)findViewById(R.id.btntelefono1);
        btntelefono2 = (Button)findViewById(R.id.btntelefono2);

        btnmapa = (Button)findViewById(R.id.btnmapa);
        btnmapa1 = (Button)findViewById(R.id.btnmapa1);
        btnmapa2 = (Button)findViewById(R.id.btnmapa2);

        btnmapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapa = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(mapa);
            }
        });

        btnmapa1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapa = new Intent(getApplicationContext(), MapsActivity2.class);
                startActivity(mapa);
            }
        });

        btnmapa2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapa = new Intent(getApplicationContext(), MapsActivity3.class);
                startActivity(mapa);
            }
        });

        btntelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numero = telefono.getText().toString();;
                if(numero!=null){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(numero);
                    }
                }
            }
            private void versionesAnteriores (String numero)
            {
                Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numero));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(llamada);
                }else{
                    Toast.makeText(Locales.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btntelefono1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numero1 = telefono1.getText().toString();;
                if(numero1!=null){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE1);
                    }else{
                        versionesAnteriores(numero1);
                    }
                }
            }
            private void versionesAnteriores (String numero1)
            {
                Intent llamada1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numero1));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(llamada1);
                }else{
                    Toast.makeText(Locales.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btntelefono2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numero2 = telefono2.getText().toString();;
                if(numero2!=null){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE2);
                    }else{
                        versionesAnteriores(numero2);
                    }
                }
            }
            private void versionesAnteriores (String numero2)
            {
                Intent llamada2 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numero2));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(llamada2);
                }else{
                    Toast.makeText(Locales.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }

        });


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode)
        {
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)) {
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        String NumeroTelefono = telefono.getText().toString();

                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + NumeroTelefono));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                            return;
                        startActivity(llamada);
                    } else {
                        Toast.makeText(Locales.this, "No aceptaste los permisos", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case PHONE_CALL_CODE1:
                String permission1 = permissions[0];
                int result1 = grantResults[0];
                if (permission1.equals(Manifest.permission.CALL_PHONE)) {
                    if (result1 == PackageManager.PERMISSION_GRANTED) {
                        String NumeroTelefono1 = telefono1.getText().toString();
                        Intent llamada1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + NumeroTelefono1));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                            return;
                        startActivity(llamada1);
                    } else {
                        Toast.makeText(Locales.this, "No aceptaste los permisos", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case PHONE_CALL_CODE2:
                String permission2 = permissions[0];
                int result2 = grantResults[0];
                if (permission2.equals(Manifest.permission.CALL_PHONE)) {
                    if (result2 == PackageManager.PERMISSION_GRANTED) {
                        String NumeroTelefono2 = telefono2.getText().toString();
                        Intent llamada2 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + NumeroTelefono2));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                            return;
                        startActivity(llamada2);
                    } else {
                        Toast.makeText(Locales.this, "No aceptaste los permisos", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
    private boolean verificarPermisos (String permiso)
    {
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}