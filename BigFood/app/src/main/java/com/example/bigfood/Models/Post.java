package com.example.bigfood.Models;

import com.example.bigfood.ListaUsuario;
import com.example.bigfood.Usuario;

import com.example.bigfood.remote.ApiUtils;
import com.example.bigfood.remote.CRegistro;
import com.example.bigfood.remote.Uwu;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Vector;

public class Post {

    @SerializedName("Correo")
    @Expose
    private String Correo;
    private Boolean estado;

    @SerializedName("Nombre")
    @Expose
    private String Nombre;
    @SerializedName("Contraseña")
    @Expose
    private String Contraseña;

    @SerializedName("Numero")
    @Expose
    private Integer Numero;

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {this.Correo = correo; }

    public String getNombre() {return Nombre;}

    public void setNombre(String nombre) {this.Nombre = nombre;}

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String contraseña) {Contraseña = contraseña;}

    public Integer getNumero() {
        return Numero;
    }

    public void setNumero(Integer numero) {this.Numero = numero;}

    public Boolean getEstado() {return estado;    }
    public void setEstado(Boolean estado) {this.estado = estado;}

}
