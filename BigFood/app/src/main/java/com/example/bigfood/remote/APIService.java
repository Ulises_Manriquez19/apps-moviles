package com.example.bigfood.remote;
import com.example.bigfood.Login;
import com.example.bigfood.Models.LoginModel;
import com.example.bigfood.Models.Post;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {
    @POST("index.php?action=registrar&controller=Usuario")
    @FormUrlEncoded
    Call<LoginModel> savePost(@Field("Correo") String Correo,
                        @Field("Nombre") String Nombre,
                        @Field("Contraseña") String Contraseña,
                        @Field("Numero") String Numero);
    @POST("index.php?action=Login&controller=Usuario")
    @FormUrlEncoded
    Call<LoginModel> login(@Field("Correo")String Nombre,
                           @Field("Contraseña") String Contraseña);
}

