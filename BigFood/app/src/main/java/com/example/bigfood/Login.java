package com.example.bigfood;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bigfood.Models.LoginModel;
import com.example.bigfood.remote.Uwu;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    EditText TextUsuario, TextContraseña;
    Button btnIniciar, btnRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextUsuario = (EditText) findViewById(R.id.TextUsuario);
        TextContraseña = (EditText) findViewById(R.id.TextContraseña);

        btnIniciar = (Button) findViewById(R.id.btnIniciar);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);

        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String usuario = TextUsuario.getText().toString();
                String contraseña = TextContraseña.getText().toString();
                if (TextUsuario.getText().toString().isEmpty()){
                    Toast.makeText(Login.this,"Campo Correo esta vacio ", Toast.LENGTH_LONG).show();
                }else {
                    if (TextContraseña.getText().toString().isEmpty()){
                        Toast.makeText(Login.this,"Campo Contraseña esta vacio ", Toast.LENGTH_LONG).show();
                    }else{
                        Uwu peticiones=new Uwu();
                        Call<LoginModel> peticion=peticiones.api.login(usuario,contraseña);
                        peticion.enqueue(new Callback<LoginModel>() {
                            @Override
                            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                                if (response.isSuccessful()){
                                    LoginModel owo= response.body();
                                    if (owo.getEstado()){
                                        Intent Inicio = new Intent(getApplicationContext(), Locales.class);
                                        startActivity(Inicio);
                                    }else{
                                        Toast.makeText(getApplicationContext(), "No estas registrado querido amigo mio", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                            @Override
                            public void onFailure(Call<LoginModel> call, Throwable t) {

                            }
                        });
                    }
                }

            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registro = new Intent(getApplicationContext(), Registro.class);
                startActivity(registro);
            }
        });
    }
}