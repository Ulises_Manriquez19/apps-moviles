package com.example.bigfood.Models;

import com.example.bigfood.Usuario;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("estatus")
    @Expose
    private Boolean estado;
    @SerializedName("usuario")
    @Expose
    private Usuario detalle;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Usuario getDetalle() {
        return detalle;
    }

    public void setDetalle(Usuario detalle) {
        this.detalle = detalle;
    }
}
