package com.example.bigfood;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bigfood.Models.LoginModel;
import com.example.bigfood.Models.Post;
import com.example.bigfood.remote.APIService;
import com.example.bigfood.remote.CRegistro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {
    
    private EditText usuario, Contraseña, Correo, Numero;
    private TextView responsable;
    Button btnRegistrar, btnLogin;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        Correo = (EditText) findViewById(R.id.Correo);
        usuario = (EditText) findViewById(R.id.usuario);
        Contraseña = (EditText) findViewById(R.id.Contraseña);
        Numero = (EditText) findViewById(R.id.Numero);
        


       btnRegistrar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String pUsuario = usuario.getText().toString();
               String pContraseña = Contraseña.getText().toString();
               String pCorreo = Correo.getText().toString();
               String pNumero=Numero.getText().toString();

               if(Correo.getText().toString().isEmpty()){
                   Toast.makeText(Registro.this,"Campo Correo esta vacio ", Toast.LENGTH_LONG).show();
               }else{
                   if(usuario.getText().toString().isEmpty()){
                       Toast.makeText(Registro.this,"Campo Usuario esta vacio ", Toast.LENGTH_LONG).show();
                   }else{
                       if(Contraseña.getText().toString().isEmpty()){
                           Toast.makeText(Registro.this,"Campo Contraseña esta vacio ", Toast.LENGTH_LONG).show();
                       }else{
                           if(Numero.getText().toString().isEmpty()){
                               Toast.makeText(Registro.this,"Campo numero esta vacio ", Toast.LENGTH_LONG).show();
                           }else{
                               if(Contraseña.getText().toString().isEmpty()){
                                   Toast.makeText(Registro.this,"Campo Contraseña esta vacio ", Toast.LENGTH_LONG).show();
                               }else{
                                   CRegistro cr = new CRegistro();
                                   Call<LoginModel> peticion=cr.api.savePost(pCorreo,pUsuario,pContraseña,pNumero);
                                   peticion.enqueue(new Callback<LoginModel>() {
                                       @Override
                                       public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                                           if (response.isSuccessful()){
                                               LoginModel owo= response.body();
                                               if (owo.getEstado()){
                                                   Intent Inicio = new Intent(getApplicationContext(), Login.class);
                                                   startActivity(Inicio);
                                               }else{
                                                   Toast.makeText(getApplicationContext(), "No te puedes registrar", Toast.LENGTH_LONG).show();
                                               }
                                           }
                                       }
                                       @Override
                                       public void onFailure(Call<LoginModel> call, Throwable t) {

                                       }
                                   });
                               }
                           }
                       }
                   }
               }


           }
       });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MaintActivity = new Intent(getApplicationContext(), Login.class);
                startActivity(MaintActivity);
            }
        });
    }

}