package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList <String> Palabras = new ArrayList<>();
    // se declara un arrayList que contendra todas las palbras del crucigrama
    int a=0;
    // se declara una variable entera inicializada en 0
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button  btn = (Button)findViewById(R.id.btnAgregar);
        //se crean la variable button para la asignacion de cada  palabra mediante el presionado del mismo
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //se castea la variable creada con anterioridad


                EditText caja = (EditText) findViewById(R.id.editText);
                //aqui mismo se crea el edittext el cual utilizaremos para poner las palabras
                TextView textViewPalabra1 = (TextView) findViewById(R.id.textView4);
                //se inicia la variable  para asi mostrarse en el textview conrrespondiente
                if(a<=6){
                    //se inicia el if  hasta el numero maximo que seria 6
                    Palabras.add(caja.getText().toString());
                    //aqui dentro del if se añadiran las palabras que hayamos puesto con anteriodad
                    a++;
                    //añadira de una en una
                }else{
                    Toast.makeText(MainActivity.this, "Limite alcanzado", Toast.LENGTH_SHORT).show();
                    //se imprime en pantalla un mensaje con el limite de palabras alcanzado
                }


            }
        });
        Button Btn=(Button)findViewById(R.id.button2);
        //se crea la variable del boton que realizara la accion de mostrar
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //se caste la variable
                TextView textViewPalabra1 = (TextView) findViewById(R.id.textView);
                textViewPalabra1.setText(Palabras.get(0));
                TextView textViewPalabra2 = (TextView) findViewById(R.id.textView2);
                textViewPalabra2.setText(Palabras.get(1));
                TextView textViewPalabra3 = (TextView) findViewById(R.id.textView3);
                textViewPalabra3.setText(Palabras.get(2));
                TextView textViewPalabra4 = (TextView) findViewById(R.id.textView4);
                textViewPalabra4.setText(Palabras.get(3));
                TextView textViewPalabra5 = (TextView) findViewById(R.id.textView5);
                textViewPalabra5.setText(Palabras.get(4));
                TextView textViewPalabra6 = (TextView) findViewById(R.id.textView6);
                textViewPalabra6.setText(Palabras.get(5));
                //se crea un textview para cada palabra y asi mismo PARA mostrarlo en cada textview
                //correspondientemente y se asigna una posicion del arraylist para cada texview y asi
                //mostrarlo de manera correcta
            }
        });


    }
}