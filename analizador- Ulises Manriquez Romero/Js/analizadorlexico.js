
function entrada()
//se crea una primer funcion que sera la que ingreso o entrada 
{
var expresion= document.getElementById('expresion').value;
var resultado= document.getElementById('resultado');
//se crean dos variable que seran utlizadas para la expresion y el primer resultado 



    var array = expresion.split(" ");
    // se crea otra variable para el arreglo  que sera igual a la expresion .split(el cual separa el string en un arreglo)

    for(var x in array)
    //comenzamos con el recorrido del arreglo 
    {
        if(/[0-9]{3,5}/.test(array[x]))
        //sentencia if para aceptacion de solo numeros
        {
            resultado.innerHTML=resultado.innerHTML+array[x] + " numero\n";
            
            //de modo que si se coloca un numero dentro de textarea se mostrara junto a la leyenda que aparece 
        }
        else if(/[+]|[*]|[/]|[()]|[-]|[=]/.test(array[x]))
        //sentencia else if por si no es la de arriba que sea la de abajo 
        {
            resultado.innerHTML=resultado.innerHTML+array[x] + " operadores\n";
            //al igual que arriba se mostra dentro del text area junto a la leyenda 
        }
        else if(/[A-Z]/.test(array[x]))
        //sentencia else if por si no es la de arriba que sea la de abajo 
        {
            resultado.innerHTML=resultado.innerHTML+array[x] + " letra\n";
            //al igual que arriba se mostra dentro del text area junto a la leyenda 
        }
        else if (/for|if|else|switch|case|break|var/.test(array[x]))
        //sentencia else if por si no es la de arriba que sea la de abajo 
        {
            resultado.innerHTML=resultado.innerHTML+array[x] + " palabras reservadas\n";
            //al igual que arriba se mostra dentro del text area junto a la leyenda 
        }
        else
        //sentencia else para si no es ninguno de los dos es este 
        {
            resultado.innerHTML=resultado.innerHTML+array[x] + " nada\n";
            //mostrara la leyenda de que no se ingreso nada 
        }

    }
}

function salida()
//se crea una segunda funcion que sera la de salida la cual nos dara si esta correcta o no la cadena 
{
    var expresion= document.getElementById('expresion').value;
    var salida= document.getElementById('salida');
    //se crean al igual que la anterior dos variable la de expresion y en este caso salida 


    var array2 = expresion.split(" ");
    // se crea otra variable para el arreglo  que sera igual a la expresion .split(el cual separa el string en un arreglo)

    

    for(var y in array2)
     //comenzamos con el recorrido del arreglo 
    {
        
        if(/^([(0-9])?[+\-*\/0-9]+([+*\-\/)])?[+*\-\/0-9]+([=])[0-9]/.test(array2[y]))
        //se crea el if con valores que reconocera de manera de expresion regular 
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
             //de modo que si se coloca lo que se pide y cumple la sentencia dentro de textarea se mostrara junto a la leyenda que aparece 
        }
        else if(/^([0-9][*\-\/+])+([0-9])/.test(array2[y]))
        //sentencia else if con otra manera de expresion que igual sera correcta
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
            //de modo que si se coloca lo que se pide y cumple la sentencia dentro de textarea se mostrara junto a la leyenda que aparece 
        }
        else if(/^[0-9][+\/\-*]+[0-9]/.test(array2[y]))
        //sentencia else if con una ultima manera de expresion que es igual de manera correcta 
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
            //de modo que si se coloca lo que se pide y cumple la sentencia dentro de textarea se mostrara junto a la leyenda que aparece 
        }
        else if(/[a-z]+[\s][A-Z][=][0-9][;]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[a-z]+[()]+[A-Z]+[\=<]+[0-9][\;][A-Z][\>=]+[0-9]{2,5}[\;][A-Z][\+][\+]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }   
        else if(/[\{]+[a-z][a-z][()][A-Z][=][=][0-9][()]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[A-Z]+[=]["][A-Z]["][;]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[}][a-z][a-z][a-z][a-z][{]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[A-Z]+[=]["][A-Z]["][;][}]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[a-z]+[(][a-z]{2,15}[)][{]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[a-z]+[/s][a-z][=][A-Z]{2,15}[0-9][:]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[a-z]+[.][a-z]{1,3}[(]["][a-z]{3,20}["][)][;]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else if(/[a-z]+[;][}]/.test(array2[y]))
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es correcta\n";
        }
        else
        //sentencia else que si no se cumple alguna de las de arriba entrara en esta 
        {
            salida.innerHTML=salida.innerHTML+array2[y] + " La cadena ingresada es incorrecta\n";
            //de modo que si no se  coloca lo que se pide y no se  cumple la sentencia dentro de textarea se mostrara junto a la leyenda que aparece 

        }

    }

}