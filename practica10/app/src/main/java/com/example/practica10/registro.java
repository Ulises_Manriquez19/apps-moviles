package com.example.practica10;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class registro extends AppCompatActivity {

    private EditText usuario, contraseña, TextUsuario, TextContraseña;
    Button btnregistrar, btnInicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        btnregistrar = (Button)findViewById(R.id.btnregistrar);
        btnInicio = (Button)findViewById(R.id.btnInicio);

        usuario=(EditText)findViewById(R.id.usuario);
        contraseña=(EditText)findViewById(R.id.contraseña);

        btnregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pUsuario = usuario.getText().toString();
                String pContraseña = contraseña.getText().toString();
                Usuario obj = new Usuario();
                if (Usuario.verificarUsuariosNuevos(pUsuario)==-1)
                {
                    obj.setUsuario(pUsuario);
                    obj.setContraseña(pContraseña);
                    ListaUsuario.agregar(obj);
                    Toast.makeText(registro.this, "Ya te registraste correctamente prro", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(registro.this, "este usuario ya esta registrado >:c", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MaintActivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(MaintActivity);
            }
        });
    }
}