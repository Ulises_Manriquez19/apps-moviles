package com.example.practica10;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText TextUsuario, TextContraseña;
    Button btnIniciar, btnregistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextUsuario = (EditText)findViewById(R.id.TextUsuario);
        TextContraseña = (EditText)findViewById(R.id.TextContraseña);

        btnIniciar = (Button)findViewById(R.id.btnIniciar);
        btnregistro = (Button)findViewById(R.id.btnregistro);

        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = TextUsuario.getText().toString();
                String contraseña = TextContraseña.getText().toString();
                Usuario usuario1=new Usuario();
                int pos = Usuario.verificarInicio(usuario,contraseña);
                if (pos ==-1)
                {
                    Toast.makeText(getApplicationContext(),"no estas registrado prro", Toast.LENGTH_LONG).show();
                }else{
                    Intent Inicio = new Intent(getApplicationContext(), Inicio.class);
                    startActivity(Inicio);
                }
            }
        });
        btnregistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent registro = new Intent(getApplicationContext(), registro.class );
                startActivity(registro);
            }
        });
    }

}