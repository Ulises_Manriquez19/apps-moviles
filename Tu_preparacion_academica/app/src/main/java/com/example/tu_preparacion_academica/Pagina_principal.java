package com.example.tu_preparacion_academica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Pagina_principal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void software (View view)
    {
        Intent software = new Intent(this, Ing_Software.class );
        startActivity(software);
    }
    public void mecanica (View view)
    {
        Intent mecanica = new Intent(this, Ing_Mecanica.class );
        startActivity(mecanica);
    }
    public void industrial (View view)
    {
        Intent industrial = new Intent(this, Ing_Industrial.class );
        startActivity(industrial);
    }
    public void negocios (View view)
    {
        Intent negocios = new Intent(this, Lic_NegociosInter.class );
        startActivity(negocios);
    }
    public void Finanzas (View view)
    {
        Intent finanzas = new Intent(this, Lic_FinanzasEco.class );
        startActivity(finanzas);
    }
    public void derecho (View view)
    {
        Intent derecho = new Intent(this, Lic_Derecho.class );
        startActivity(derecho);
    }


}