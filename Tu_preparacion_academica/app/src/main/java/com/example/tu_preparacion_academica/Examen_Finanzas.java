package com.example.tu_preparacion_academica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Examen_Finanzas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examen__finanzas);
    }
    public void Menu (View view)
    {
        Intent Menu = new Intent(this, Pagina_principal.class );
        startActivity(Menu);
    }
}