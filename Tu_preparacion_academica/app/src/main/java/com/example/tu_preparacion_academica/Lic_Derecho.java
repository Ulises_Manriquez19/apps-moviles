package com.example.tu_preparacion_academica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Lic_Derecho extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lic__derecho);
    }

    public void menu (View view)
    {
        Intent menu = new Intent(this, Pagina_principal.class );
        startActivity(menu);
    }
    public void examen (View view)
    {
        Intent examen = new Intent(this, Examen_Derecho.class );
        startActivity(examen);
    }
}