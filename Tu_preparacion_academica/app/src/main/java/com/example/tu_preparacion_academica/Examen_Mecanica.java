package com.example.tu_preparacion_academica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Examen_Mecanica extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examen__mecanica);
    }
    public void menu (View view)
    {
        Intent menu = new Intent(this, Pagina_principal.class );
        startActivity(menu);
    }
}